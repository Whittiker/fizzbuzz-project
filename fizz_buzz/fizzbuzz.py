def fizz_buzz(num):
    if num%3 == 0 and num%7 == 0 and num%5 == 0:
        return 'FizzBuzzPop'
    elif num%3 == 0 and num%5 == 0:
        return 'FizzBuzz'
    elif num%3 == 0 and num%7 ==0:
        return 'FizzPop'
    elif num%5 == 0 and num%7 == 0:
        return 'BuzzPop'
    elif num%5 == 0:
        return 'Buzz'
    elif num%3 == 0:
        return 'Fizz'
    elif num%7 == 0:
        return 'Pop'
    return num