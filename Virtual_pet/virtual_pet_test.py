import unittest
from virtual_pet import *

class virtual_pet_test(unittest.TestCase):
    def setUp(self):
        print('Start before the test')

    def tearDown(self):
        print('Print after the test')

    def test_get_hungriness(self):
        pet = virtual_pet()
        self.assertEqual(pet.get_hungriness, 50)
    
    def test_get_fullness(self):
        pet = virtual_pet()
        self.assertEqual(pet.get_fullness, 50)

    def test_feed(self):
        pet = virtual_pet()
        self.assertEqual(pet.feed(), (40, 60))

    def test_play(self):
        pet = virtual_pet()
        self.assertEqual(pet.play(), (10, 10))

    def test_sleep(self):
        pet = virtual_pet()
        self.assertEqual(pet.sleep(), 0)
    
    def test_poop(self):
        pet = virtual_pet()
        self.assertEqual(pet.poop(), 40)

if __name__ == '__main__':
    unittest.main()

